;;; ukrainian-calendar.el --- Український календар -*- lexical-binding: t; -*-

;; Copyright (c) 2022 Andrij Mizyk
;;
;; За основу взято `ukrainian-holidays.el' від Oleh Krehel
;; https://github.com/abo-abo/ukrainian-holidays
;; Copyright (c) 2012 Oleh Krehel
;;
;; This file is not part of GNU Emacs.
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;; Встановлення:
;;
;; Cкопіюйте цей файл у теку, що належить до `load-path' і додайте у свій
;; `.emacs' наступні рядки:
;;
;;     (require 'ukrainian-calendar)
;;
;;; Код:

(setq calendar-week-start-day 1)
(setq calendar-date-style 'european)  ; день/місяць/рік

(setq calendar-date-display-form
  '((if dayname (concat dayname ", "))
    day " " (substring monthname 0 3) ". " year))

(setq calendar-day-name-array
  ["Неділя" "Понеділок" "Вівторок" "Середа" "Четвер" "Пʼятниця" "Субота"])

(setq calendar-day-abbrev-array
  ["Нд" "Пн" "Вт" "Ср" "Чт" "Пт" "Сб"])

(setq calendar-day-header-array
  ["Нд" "Пн" "Вт" "Ср" "Чт" "Пт" "Сб"])

(setq calendar-month-name-array
  ["Січень" "Лютий"   "Березень" "Квітень" "Травень"  "Червень"
   "Липень" "Серпень" "Вересень" "Жовтень" "Листопад" "Грудень"])

(setq calendar-month-abbrev-array
  ["січ" "лют" "бер" "кві" "тра" "чер" "лип" "сер" "вер" "жов" "лис" "гру"])

;; lunar.el
(setq lunar-phase-names '("Новик" "Молодик" "Повня" "Старик"))

;; solar.el
(setq
 calendar-time-display-form '(24-hours ":" minutes)
 solar-n-hemi-seasons
  '("Весняне рівнодення" "Літнє сонцестояння"
     "Осіннє рівнодення" "Зимове сонцестояння")
 solar-s-hemi-seasons
  '("Осіннє рівнодення" "Зимове сонцестояння"
    "Весняне рівнодення" "Літнє сонцестояння"))

;; Українські святкові дні

(defvar holiday-ukrainian-holidays nil "Ukrainian holidays")
(defalias 'holiday-orthodox-easter 'holiday-greek-orthodox-easter)

(setq holiday-ukrainian-holidays
      `(
	;; січень
	(holiday-fixed 1 1 "Новий рік / Василя (за н.к.)")
	(holiday-fixed 1 6 "Святий вечір / Водохреща (за н.к.)")
	(holiday-fixed 1 7 "Різдво Христове")
	(holiday-fixed 1 14 "Василія Великого св.")
	(holiday-fixed 1 19 "Водохреща")
	(holiday-fixed 1 22 "День Соборності України")
	(holiday-fixed 1 29 "День памʼяті Героїв Крут")

	;; лютий
	(holiday-fixed 2 15 "Стрітення")
	(holiday-fixed 2 20 "День Героїв Небесної Сотні")

	;; березень
	(holiday-fixed 3 8 "Міжнародний жіночий день")
	(holiday-fixed 3 25 "Благовіщення (за н.к.)")

	;; квітень
	(holiday-fixed 4 7 "Благовіщення")
	(holiday-fixed 4 23 "Юрія (за н.к.)")

	;; травень
	;(holiday-fixed 5 1 "День праці")
	(holiday-float 5 0 2 "День матері")
	;(holiday-fixed 5 9 "День перемоги")
	(holiday-float 5 4 3 "День вишиванки")
	(holiday-fixed 5 6 "Юрія")

	;; червень
	(holiday-fixed 6 24 "Івана Купала (Різдво св. Івана Хрестителя) (за н.к.)")
	(holiday-fixed 6 28 "День Конституції України")
	(holiday-fixed 6 29 "Петра й Павла верховних апп. (за н.к.)")

	;; липень
	(holiday-fixed 7 7 "Івана Купала (Різдво св. Івана Хрестителя)")
	(holiday-fixed 7 12 "Петра й Павла верховних апп.")
	(holiday-fixed 7 20 "Іллі (за н.к.)")

	;; серпень
	(holiday-fixed 8 1 "Маковея (Медовий Спас) (за н.к.)")
	(holiday-fixed 8 2 "Іллі")
	(holiday-fixed 8 6 "Преображення Господнє (Яблучний Спас) (за н.к.)")
	(holiday-fixed 8 7 "Успення св. Анни")
	(holiday-fixed 8 14 "Маковея (Медовий Спас)")
	(holiday-fixed 8 28 "Успіння Пресвятої Богородиці (за н.к.)")
	(holiday-fixed 8 19 "Преображення Господнє (Яблучний Спас)")
	(holiday-fixed 8 23 "День Державного прапора України")
	(holiday-fixed 8 24 "День незалежності України")
	(holiday-fixed 8 28 "Успіння Пресвятої Богородиці")

	;; вересень
	(holiday-fixed 9 8 "Успіння Пресвятої Богородиці (за н.к.)")
	(holiday-fixed 9 14 "Воздвиження (за н.к.)")
	(holiday-fixed 9 21 "Успіння Пресвятої Богородиці")
	(holiday-fixed 9 27 "Воздвиження")

	;; жовтень
	(holiday-fixed 10 1 "Покрови Пресвятої Богородиці (за н.к.)")
	(holiday-fixed 10 14 "День захисника України / Покрови Пресвятої Богородиці")
	(holiday-fixed 10 26 "Дмитрів день (за н.к.)")

	;; листопад
	(holiday-fixed 11 8 "Дмитрів день")
	(holiday-fixed 11 21 "День Гідності та Свободи / Михайлів день / Введення (за н.к.)")
	(holiday-float 11 6 4 "День памʼяті жертв голодоморів")
	(holiday-fixed 11 26 "Юрія")
	(holiday-fixed 11 30 "Андрія (за н.к.)")

	;; грудень
	(holiday-fixed 12 4 "Введення у храм Пресв. Богородиці")
	(holiday-fixed 12 6 "Миколая (за н.к.)")
	(holiday-fixed 12 9 "Юрія")
	(holiday-fixed 12 13 "Андрія Первозванного ап.")
	(holiday-fixed 12 19 "Миколая чудотворця")
	(holiday-fixed 12 25 "Різдво Христове (за н.к.)")
	(holiday-fixed 12 31 "Щедрий вечір (Меланки) (за н.к.)")

	;; ----------------------------------------------------------------

	;; зі змінною датою
	(holiday-orthodox-easter -7 "Вербна неділя")
	(holiday-orthodox-easter -3 "Великий четвер (cтрасті)")
	(holiday-orthodox-easter -2 "Велика пʼятниця (Плащаниця)")
	(holiday-orthodox-easter -1 "Велика субота")
	(holiday-orthodox-easter 0 "Великдень (Пасха)")
	(holiday-orthodox-easter 1 "Світлий понеділок")
	(holiday-orthodox-easter 2 "Світлий вівторок")
	(holiday-orthodox-easter 39 "Вознесіння")
	(holiday-orthodox-easter 49 "Трійця")
	))

;; показувати лише українські свята
(setq calendar-holidays holiday-ukrainian-holidays)

(provide 'ukrainian-calendar)
;;; ukrainian-calendar.el ends here
